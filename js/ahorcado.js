/* JS DOCUMENT */

var a=["caracter","huerto","clases","ratones"];
var numero=obtenerPalabra();
var palabra=a[numero];

var contadorAciertos=0;
var contadorFallos=0;
var caracteresIntroducidos=[];

console.log(palabra);

function cargar(){
    var caja1 = document.querySelectorAll(".wrapper>div")[0];
    var caja2 = document.querySelectorAll(".wrapper>div")[1];

    caja1.className="aciertos";
    caja2.setAttribute("class", "fallos");

    creadiv(palabra);
}

function creadiv(arg) {
    var longitud;
    longitud = arg.length;
    console.log(longitud);


    for (var c = 0; c < longitud; c++) {
        document.querySelector(".aciertos").innerHTML += '<div class="letras"></div>';
    }
}



function control(){
    var letra;
    var posiciones;
    var yaEsta=0;

    letra=document.querySelector("#introduce").value;

    
    for(var c=0;c<caracteresIntroducidos.length;c++){
        if(caracteresIntroducidos[c]==letra.toLowerCase()){
            yaEsta=1;
        }
    }

    if(yaEsta==0){
        caracteresIntroducidos.push(letra);
        posiciones=comprobar(letra);
    
        if(posiciones.length==0){
            dibujarFallo(letra);
        }else{
            dibujarAcierto(letra,posiciones);
        }       
    }
}


function dibujarAcierto(letra,posiciones){
    var f;
    var c;

    /* Incremento el contador de aciertos */
    contadorAciertos+=posiciones.length;

    console.log(contadorAciertos);

    f=document.querySelectorAll(".aciertos>.letras");

    for(c=0;c<posiciones.length;c++){
        f[posiciones[c]].innerHTML=letra;
    }

    if(contadorAciertos==palabra.length){
        setTimeout(
            function(){
                alert("Has ganado!!")
                location.reload();
            },
            1000
            );
    }
}

function dibujarFallo(letra){
    var f;

    f=document.querySelector(".fallos");
    f.innerHTML += '<div class="letras">' + letra + '</div>';

    /* Incremento el contador de fallos */
    // contadorFallos=letra++;

    // console.log(contadorFallos);     //  Está mal, no funciona

    // if(contadorFallos==6){
    //     alert("Te han ahorcado");
    // }
}

function comprobar(caracter){
    var c=0;
    var posiciones=[];

    for(c=0;c<palabra.length;c++){
        if(caracter.toLowerCase()==palabra[c]){
            posiciones.push(c);
        }
    }

    // Si sale del for y posiciones esta vacio es que el caracter no esta.
    
    return posiciones;

function resetear(){
    window.location.reload();
}


}

function obtenerPalabra(){
    return parseInt(Math.random()*a.length);
}
